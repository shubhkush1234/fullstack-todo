import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  errorMsg= 'Some error occured. Contact admin @ shubhkush1234@gmail.com';

  constructor(  ) { }

  ngOnInit() {
  }

}
