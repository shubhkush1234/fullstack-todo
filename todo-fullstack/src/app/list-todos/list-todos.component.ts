import { Component, OnInit } from '@angular/core';


export class Todo{
  constructor(
    public id: number,
    public description: String,
    public isDone: boolean,
    public targetDate: Date
  ){ }
};

@Component({
  selector: 'app-list-todos',
  templateUrl: './list-todos.component.html',
  styleUrls: ['./list-todos.component.css']
})

export class ListTodosComponent implements OnInit {

  todos = [ 
    new Todo(1, 'learn to dance', false, new Date()),
    new Todo(2, 'learn to play sudoku', false, new Date()),
    new Todo(3, 'learn Sping Boot', false, new Date()),
  ]

  constructor() { }

  ngOnInit() {
  }

}
