import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HardcodedAuthServiceService } from '../service/hardcoded-auth-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  errorMsg = 'Oops....Invalid Credentials';
  successMsg = 'Login successful!!!'
  loginFail = false;

  constructor(private router: Router,
    private HardcodedAuthServiceService: HardcodedAuthServiceService

    ) { }

  handleLogin() {
    if( this.HardcodedAuthServiceService.authenticate(this.username, this.password )){

    // if (this.username === 'shubham' && this.password === 'shubham1234') {
      this.loginFail = false;
      this.router.navigate([ 'welcome' , this.username])
    }
     else {
      this.loginFail = true;
    }
  }

  ngOnInit() {
  }

}
