import { Component, OnInit } from '@angular/core';
import { HardcodedAuthServiceService } from '../service/hardcoded-auth-service.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private hardcodedAuthServiceService: HardcodedAuthServiceService) { }

  // isUserLoggedIn: boolean = false

  ngOnInit() {
    // this.isUserLoggedIn = this.hardcodedAuthServiceService.isUserLoggedIn();
  }

}
