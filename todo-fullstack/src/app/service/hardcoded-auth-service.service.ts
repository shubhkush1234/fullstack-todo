import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HardcodedAuthServiceService {

  constructor() { }

  authenticate(username, password) {
    if (username === 'shubham' && password === 'shubham1234') {
// to see its effect, goto session storage, clear it and then hard reload.
      console.log("Before: " + this.isUserLoggedIn());
      sessionStorage.setItem('authenticaterUser', username);
      console.log("After: " + this.isUserLoggedIn());

      return true;
    }
    return false;
  }
  logout(){
    sessionStorage.removeItem('authenticaterUser');

  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('authenticaterUser');
    return !(user === null)
  }
}
